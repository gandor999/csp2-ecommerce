const User = require("../models/User");
const Product = require('../models/Product');
const Order = require('../models/Order');
const bcrypt = require("bcrypt");
const auth = require('../auth');



// Register User
module.exports.registerUser = (reqBody) => {

	let newUser = new User({
		email : reqBody.email,
		password : bcrypt.hashSync(reqBody.password, 10)
	});

	return newUser.save()
	.then((promise, error) => {
		if(error){
			return false;
		}
		else{
			return `User is now registered`;
		}
	})
}



// User login
module.exports.loginUser = (reqBody) => {
	return User.findOne({email: reqBody.email})
	.then(result => {
		if(result == null){
			return `No such user`;
		}
		else{
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

			if(isPasswordCorrect){
				return {access: auth.createAccessToken(result)}
			}
			else{
				return `error`;
			}
		}
	})
}




// Set admin
module.exports.setAdmin = async (data, isAdmin) => {

	if(isAdmin){
		let update = {
			isAdmin: true
		}

		return User.findByIdAndUpdate(data, update)
		.then((promise, error) => {
			if(error){
				return false;
			}
			else{
				return `User has been given admin permission`;
			}
		})
	}

	else{
		return `Admin authority only`;
	}
}
