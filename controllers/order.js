const Product = require('../models/Product');
const Order = require('../models/Order');
const bcrypt = require("bcrypt");
const auth = require('../auth');


// Checkout
module.exports.checkout = async (data, user) => {


	if(!user.isAdmin){
		return Product.findOne({name: data.productName})
		.then((result) => {
			if(!result){
				return `Product does not exist. Please check spelling and spaces`;
			}

			else{

				if(result.isActive){
					console.log(result);
					let newOrder = new Order({
						totalAmount : data.totalAmount,
						userId : user.id,
						productName: data.productName,
						productId: result.id
						
					});

					return newOrder.save()
					.then((promise, error) => {
						if(error){
							return `Order was not created`;
						}
						else{
							return `New order created`;
						}
					})
				}

				else{
					return 'Product no longer available'
				}

			}
		})
	}

	else{
		return `Admin is not allowed to checkout`;
	}

}


// Get user's orders
module.exports.getMyOrders = (data) => {
	return Order.find({userId: data.id})
	.then(result => {
		if(result == []){
			return `You have no pending orders`;
		}
		else{
			return result;
		}
	})
}



// Get all orders made
module.exports.getAllOrders =async (data) => {

	if(data.isAdmin){
		return Order.find({})
		.then(result => {
			if(result == []){
				return `You have no pending orders`;
			}
			else{
				return result;
			}
		})
	}
	else{
		return `Admin authority only`;
	}
	
}