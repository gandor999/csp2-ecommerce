const mongoose = require('mongoose');

const orderSchema = new mongoose.Schema({

	totalAmount: {
		type: Number,
		required: [true, `Amount is required`]
	},

	purchasedOn: {
		type: Date,
		default: new Date()
	},

	userId: {
		type: String,
		required: [true, `UserId is required`]
	},


	productName: {
		type: String,
		required: [true, `Product name required`]
	},

	productId: {
		type: String,
		required: [true, `UserId is required`]
	}

});


module.exports = mongoose.model("Order", orderSchema);


/*const orderSchema = new mongoose.Schema([
	{
		totalAmount: {
			type: Number,
			required: [true, `Amount is required`]
		},

		purchasedOn: {
			type: Date,
			default: new Date()
		},

		userId: {
			type: String,
			required: [true, `UserId is required`]
		},

		productId: {
			type: String,
			required: [true, `UserId is required`]
		}
	}
])*/