const express = require("express");
const router = express.Router();
const orderController = require('../controllers/order');
const auth = require('../auth');



// Create order for user
router.post("/checkout", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	orderController.checkout(req.body, userData)
	.then(resultFromController => {
		res.send(resultFromController)
	});
});



// Get all orders associated with authenticated user
router.get("/myOrders", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	orderController.getMyOrders(userData)
	.then(resultFromController => {
		res.send(resultFromController)
	});
});



// Get all orders
router.get("/allOrders", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	orderController.getAllOrders(userData)
	.then(resultFromController => {
		res.send(resultFromController)
	});
});




module.exports = router;

