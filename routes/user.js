const express = require("express");
const router = express.Router();
const userController = require('../controllers/user');
const auth = require('../auth');



// Register user
router.post("/register", (req, res) => {
	userController.registerUser(req.body)
	.then(resultFromController => {
		res.send(resultFromController)
	});
});

// Login suer
router.post("/login", (req, res) => {
	userController.loginUser(req.body)
	.then(resultFromController => {
		res.send(resultFromController)
	});
});



// Set user to admin or not
router.put("/:userId/setAdmin", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	userController.setAdmin(req.params.userId, userData.isAdmin)
	.then(resultFromController => {
		res.send(resultFromController)
	});
});


module.exports = router;


